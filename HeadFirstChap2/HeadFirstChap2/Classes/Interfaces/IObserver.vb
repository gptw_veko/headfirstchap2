﻿Public Interface IObserver
    Sub update(ByVal temp As Double, ByVal humidity As Double, ByVal pressure As Double)
End Interface
