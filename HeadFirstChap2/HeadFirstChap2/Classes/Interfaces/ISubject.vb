﻿Public Interface ISubject
    Sub removeObserver(ByRef o As IObserver)
    Sub registerObserver(ByRef o As IObserver)
    Sub notifyObserver()
End Interface