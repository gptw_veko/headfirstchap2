﻿Imports System.Collections.Generic

Public Class WeatherData
    Implements ISubject

    Private myObservers As List(Of IObserver)
    Private myTemperature As Double
    Private myHumidity As Double
    Private myPressure As Double

    Public Sub New()
        Dim myObservers As New List(Of IObserver)
    End Sub

    Public Sub notifyObserver() Implements ISubject.notifyObserver
        For Each observer As IObserver In myObservers
            observer.update(myTemperature, myHumidity, myPressure)
        Next
    End Sub

    Public Sub registerObserver(ByRef o As IObserver) Implements ISubject.registerObserver
        myObservers.Add(o)
    End Sub

    Public Sub removeObserver(ByRef o As IObserver) Implements ISubject.removeObserver
        Dim i As Integer
        i = myObservers.IndexOf(o)
        If i >= 0 Then
            myObservers.RemoveAt(i)
        End If
    End Sub

    Public Sub MeasurementsChanged()
        notifyObserver()
    End Sub

    Public Sub SetMeasurements(ByVal temperature As Double, ByVal humidity As Double, ByVal pressure As Double)
        Me.myTemperature = temperature
        Me.myHumidity = humidity
        Me.myPressure = pressure
        Me.MeasurementsChanged()
    End Sub
End Class



Public Class DisplayActualWeather
    Implements IObserver

    Implements IDisplayElement

    Private myTemp As Double
    Private myHumidity As Double
    Private myPressure As Double

    Public Sub update(temp As Double, humidity As Double, pressure As Double) Implements IObserver.update

    End Sub


    Public Sub display() Implements IDisplayElement.display
        MsgBox("Temperature: " & myTemp & vbCrLf & _
               "Humidity: " & myHumidity & vbCrLf & _
               "Pressure: " & myPressure)
    End Sub


End Class










