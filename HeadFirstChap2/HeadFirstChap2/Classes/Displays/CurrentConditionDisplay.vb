﻿Public Class CurrentConditionDisplay
    Implements IObserver
    Implements IDisplayElement

    'Member
    Private myTemperature As Double
    Private myHumidity As Double
    Private myweatherData As ISubject
    'Constructor
    Sub New(ByVal weatherData As ISubject)
        Me.myweatherData = weatherData
        weatherData.registerObserver(Me)
    End Sub

    Public Sub update(temp As Double, humidity As Double, pressure As Double) Implements IObserver.update
        myTemperature = temp
        myHumidity = humidity
        display()
    End Sub

    Public Sub display() Implements IDisplayElement.display
        MsgBox("Current conditions: " + myTemperature.ToString + "F degrees and " + myHumidity.ToString + "% humidity")
    End Sub
End Class
