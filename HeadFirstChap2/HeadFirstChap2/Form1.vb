﻿Public Class Form1

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim weatherData As New WeatherData
        Dim currentConditionsDisplay As New CurrentConditionDisplay(weatherData)
        Dim statisticsDisplay As New StatisticsDisplay(weatherData)
        Dim forecastDisplay As New ForecastDisplay(weatherData)
        Dim heatIndexDisplay As New HeatIndexDisplay(weatherData)

        weatherData.SetMeasurements(80, 65, 30.4)
        currentConditionsDisplay.display()
        statisticsDisplay.Display()
        forecastDisplay.Display()
        heatIndexDisplay.Display()
        weatherData.SetMeasurements(82, 70, 29.2)
        weatherData.SetMeasurements(78, 90, 29.2)
    End Sub
End Class
